"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const filmRoutes_1 = __importDefault(require("./routes/filmRoutes"));
const app = (0, express_1.default)();
const { PORT = process.env.PORT || 3000, } = process.env;
// app.set('views', __dirname+'/src/views/');
app.set('view engine', 'pug');
app.use(express_1.default.static(path_1.default.join(__dirname, '../../public')));
app.use('/', filmRoutes_1.default);
app.listen(PORT, () => {
    console.log('server started at http://localhost:' + PORT);
});
//# sourceMappingURL=index.js.map