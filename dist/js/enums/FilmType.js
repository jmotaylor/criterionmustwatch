"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FilmType = void 0;
var FilmType;
(function (FilmType) {
    FilmType[FilmType["LEAVING"] = 0] = "LEAVING";
    FilmType[FilmType["POPULARNOW"] = 1] = "POPULARNOW";
    FilmType[FilmType["ALLTIMEFAVORITES"] = 2] = "ALLTIMEFAVORITES";
    FilmType[FilmType["REVIEWS"] = 3] = "REVIEWS";
})(FilmType = exports.FilmType || (exports.FilmType = {}));
//# sourceMappingURL=FilmType.js.map