"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const films_1 = __importDefault(require("../controllers/films"));
const router = express_1.default.Router();
exports.default = router.get("/", (req, res) => {
    res.render('index');
});
router.get("/popular-now", films_1.default.popularNow);
router.get("/all-time-favorites", films_1.default.allTimeFavorites);
router.get("/reviews", films_1.default.reviews);
//# sourceMappingURL=films.js.map