"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AllTimeFavoritesFilm = exports.LeavingFilm = exports.PopularNowFilm = void 0;
const FilmType_1 = require("../enums/FilmType");
class Film {
    constructor(title, link, image) {
        this.title = title;
        this.link = link;
        this.image = image;
    }
}
class PopularNowFilm extends Film {
    constructor(title, link, image, type = FilmType_1.FilmType.POPULARNOW) {
        super(title, link, image);
        this.title = title;
        this.link = link;
        this.image = image;
        this.type = type;
    }
}
exports.PopularNowFilm = PopularNowFilm;
class LeavingFilm extends Film {
    constructor(title, link, image, type = FilmType_1.FilmType.LEAVING) {
        super(title, link, image);
        this.title = title;
        this.link = link;
        this.image = image;
        this.type = type;
    }
}
exports.LeavingFilm = LeavingFilm;
class AllTimeFavoritesFilm extends Film {
    constructor(title, link, image, type = FilmType_1.FilmType.ALLTIMEFAVORITES) {
        super(title, link, image);
        this.title = title;
        this.link = link;
        this.image = image;
        this.type = type;
    }
}
exports.AllTimeFavoritesFilm = AllTimeFavoritesFilm;
//# sourceMappingURL=Film.js.map