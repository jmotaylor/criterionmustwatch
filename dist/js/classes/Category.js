"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Category = void 0;
class Category {
    constructor(type, url, container) {
        this.type = type;
        this.url = url;
        this.container = container;
    }
}
exports.Category = Category;
//# sourceMappingURL=Category.js.map