"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FilmType_1 = require("../enums/FilmType");
const filmService_1 = require("../services/filmService");
class FilmController {
    constructor() { }
    static async popularNow(req, res) {
        const results = await (0, filmService_1.getResults)(FilmType_1.FilmType.POPULARNOW);
        res.render('films-list', {
            movies: results.movies,
            header: 'Leaving / Popular Now',
            description: 'Films categorized as "Popular Now" that are leaving at the end of the month.',
            noList: results.noList
        });
    }
    static async allTimeFavorites(req, res) {
        const results = await (0, filmService_1.getResults)(FilmType_1.FilmType.ALLTIMEFAVORITES);
        res.render('films-list', {
            movies: results.movies,
            header: 'Leaving / All Time Favorites',
            description: 'Films categorized as "All Time Favorites" that are leaving at the end of the month.',
            noList: results.noList
        });
    }
    static async reviews(req, res) {
        const results = await (0, filmService_1.getResultsWithReviews)();
        res.render('films-list', {
            movies: results.movies,
            header: 'Leaving Films With Tomatometer',
            description: 'Films leaving at the end of the month with scores from Rotten Tomatoes.',
            noList: results.noList
        });
    }
}
exports.default = FilmController;
//# sourceMappingURL=filmFilmController.js.map