"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const cheerio_1 = __importDefault(require("cheerio"));
const FilmType_1 = require("../enums/FilmType");
const Film_1 = require("../models/Film");
async function createLists(category) {
    resetFlags();
    while (hasMorePages) {
        try {
            await scrapeFilms(category);
            page++;
        }
        catch (err) {
            console.error('oopsie');
        }
    }
}
exports.default = createLists;
let page = 1;
let hasMorePages = true;
function resetFlags() {
    page = 1;
    hasMorePages = true;
}
async function scrapeFilms(category) {
    try {
        const { data } = await axios_1.default.get(category.url + page);
        const $ = cheerio_1.default.load(data);
        const filmItems = $(".js-collection-item");
        filmItems.each((_, el) => {
            const film = assignValuesToFilm($, el, category.type);
            if (film) {
                category.container.push(film);
            }
            else {
                throw ('FilmType is unknown.');
            }
        });
        // The site will have a "load more" button if there are more films to scrape
        hasMorePages = $('.js-load-more-link').length > 0 ? true : false;
    }
    catch (err) {
        hasMorePages = false;
        console.error('an error has occurred', err);
    }
}
function assignValuesToFilm($, el, type) {
    const title = ($(el).find(".browse-item-title").find('strong').text()).trim();
    const link = $(el).find(".browse-item-title").find('a').attr('href');
    const image = $(el).find("img").attr('src');
    let film;
    switch (type) {
        case FilmType_1.FilmType.LEAVING:
            film = new Film_1.LeavingFilm(title, link, image);
            break;
        case FilmType_1.FilmType.POPULARNOW:
            film = new Film_1.PopularNowFilm(title, link, image);
            break;
        case FilmType_1.FilmType.ALLTIMEFAVORITES:
            film = new Film_1.AllTimeFavoritesFilm(title, link, image);
            break;
        default:
            film = null;
            break;
    }
    return film;
}
//# sourceMappingURL=criterion.js.map