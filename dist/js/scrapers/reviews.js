"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const cheerio_1 = __importDefault(require("cheerio"));
const Film_1 = require("../models/Film");
async function createLists(category, leavingList) {
    try {
        const leavingFilmTitles = leavingList.map(film => film.title);
        await scrapeFilms(category, leavingFilmTitles);
    }
    catch (err) {
        console.error('oopsie');
    }
}
exports.default = createLists;
async function scrapeFilms(category, titles) {
    try {
        for (const title of titles) {
            const encodedTitle = encodeURIComponent(title);
            const url = category.url + encodedTitle;
            const { data } = await axios_1.default.get(url);
            const $ = cheerio_1.default.load(data);
            const reviewItems = $('search-page-result[slot="movie"] search-page-media-row');
            reviewItems.each((_, el) => {
                const reviewTitle = ($(el).find('a[slot="title"]').text()).trim();
                if (reviewTitle == title) {
                    // return only the very last film among duplicates because that's the oldest of the duplicates and is likely what's on criterion
                    const newReviewsContainer = removeDuplicates(category.container, title);
                    category.container = newReviewsContainer;
                    const review = assignValuesToReview($, el);
                    category.container.push(review);
                }
            });
        }
    }
    catch (err) {
        console.error('an error has occurred', err);
    }
}
function removeDuplicates(reviews, title) {
    for (let i = 0; i < reviews.length; i++) {
        if (reviews[i].title == title) {
            reviews.splice(i, 1);
        }
    }
    return reviews;
}
function assignValuesToReview($, el) {
    const title = ($(el).find('a[slot="title"]').text()).trim();
    const href = $(el).find('a[slot="thumbnail"]').attr('href');
    const percentageString = $(el).attr('tomatometerscore');
    const freshness = $(el).attr('tomatometerstate');
    // Need to convert to int to be able to use lodash orderby descending properly
    const percentage = percentageString ? parseInt(percentageString) : null;
    return new Film_1.Review(title, href, percentage, freshness);
}
//# sourceMappingURL=reviews.js.map