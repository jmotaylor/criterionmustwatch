"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.categories = void 0;
const FilmType_1 = require("./enums/FilmType");
const urls_1 = require("./urls");
const Category_1 = require("./models/Category");
const leavingFilms = [];
const popularNowFilms = [];
const allTimeFavoritesFilms = [];
exports.categories = {
    leaving: new Category_1.Category(FilmType_1.FilmType.LEAVING, urls_1.leavingUrl, leavingFilms),
    popularNow: new Category_1.Category(FilmType_1.FilmType.POPULARNOW, urls_1.popularNowUrl, popularNowFilms),
    allTimeFavorites: new Category_1.Category(FilmType_1.FilmType.ALLTIMEFAVORITES, urls_1.allTimeFavoritesUrl, allTimeFavoritesFilms),
};
//# sourceMappingURL=categories.js.map