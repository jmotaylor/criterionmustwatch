"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.rottenTomatoesUrl = exports.leavingUrl = exports.allTimeFavoritesUrl = exports.popularNowUrl = void 0;
const baseLeavingUrl = 'https://www.criterionchannel.com/leaving-';
function generateLeavingUrl() {
    const lastDayDate = getLastDayOfTheCurrentMonth();
    return baseLeavingUrl + lastDayDate + '?page=';
}
function getLastDayOfTheCurrentMonth() {
    const year = new Date().getFullYear();
    const month = new Date().getMonth() + 1;
    const lastDate = new Date(year, month, 0);
    return lastDate.toLocaleString('en-US', { month: 'long', day: 'numeric' })
        .toLowerCase()
        .replace(/ /g, "-");
}
exports.popularNowUrl = 'https://www.criterionchannel.com/popular-now?page=';
exports.allTimeFavoritesUrl = 'https://www.criterionchannel.com/all-time-favorites?page=';
exports.leavingUrl = generateLeavingUrl();
exports.rottenTomatoesUrl = 'https://www.rottentomatoes.com/search?search=';
//# sourceMappingURL=urls.js.map