"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getResults = exports.getResultsWithReviews = void 0;
const FilmType_1 = require("../enums/FilmType");
const Categories_1 = __importDefault(require("../models/Categories"));
const criterion_1 = __importDefault(require("../scrapers/criterion"));
const reviews_1 = __importDefault(require("../scrapers/reviews"));
const lodash_1 = __importDefault(require("lodash"));
async function getResultsWithReviews() {
    let categories = new Categories_1.default([], [], [], []);
    await (0, criterion_1.default)(categories.leaving);
    const leavingList = categories.leaving.container;
    await (0, reviews_1.default)(categories.reviews, leavingList);
    const leavingListWithReviews = createListWithReviews(categories);
    const filmTypeString = generateFilmTypeString(categories.leaving.type);
    return {
        movies: leavingListWithReviews,
        filmType: filmTypeString,
        noList: leavingListWithReviews.length < 1 ? ['Leaving'] : []
    };
}
exports.getResultsWithReviews = getResultsWithReviews;
function createListWithReviews(categories) {
    categories.leaving.container.forEach((film) => {
        film.review = lodash_1.default.find(categories.reviews.container, { title: film.title });
    });
    return lodash_1.default.orderBy(categories.leaving.container, [function (film) { var _a; return ((_a = film.review) === null || _a === void 0 ? void 0 : _a.percentage) || ''; }, 'title'], ['desc', 'asc']);
}
async function getResults(type) {
    const results = await generateIntersectionList(type);
    const emptyLists = emptyListHandler(results.leavingFilmsListLength, results.secondListLength, results.secondListType);
    const filmTypeString = generateFilmTypeString(results.secondListType);
    return {
        movies: results.intersectionResults,
        filmType: filmTypeString,
        noList: emptyLists
    };
}
exports.getResults = getResults;
async function generateIntersectionList(type) {
    let categories = new Categories_1.default([], [], [], []);
    await getFilms(type, categories);
    const filteredResults = filterFilms(type, categories);
    return {
        intersectionResults: filteredResults.intersectionResults,
        leavingFilmsListLength: filteredResults.leavingFilmsListLength,
        secondListLength: filteredResults.secondListLength,
        secondListType: filteredResults.secondListType
    };
}
async function getFilms(type, categories) {
    // Clear containers because even after refreshing, the old content is retained
    for (const category in categories) {
        categories[category].container = [];
    }
    const leavingCategory = categories.leaving;
    await (0, criterion_1.default)(leavingCategory);
    const secondCategory = type === FilmType_1.FilmType.POPULARNOW
        ? categories.popularNow
        : categories.allTimeFavorites;
    await (0, criterion_1.default)(secondCategory);
}
function filterFilms(type, categories) {
    let intersectionResults = [];
    let secondList;
    const leavingFilmsList = categories.leaving;
    switch (type) {
        case FilmType_1.FilmType.POPULARNOW:
            secondList = categories.popularNow;
            break;
        case FilmType_1.FilmType.ALLTIMEFAVORITES:
            secondList = categories.allTimeFavorites;
            break;
        default:
            secondList = null;
            break;
    }
    if (leavingFilmsList.container.length > 0 && secondList && secondList.container.length > 0) {
        for (const leavingFilm of leavingFilmsList.container) {
            const result = secondList.container.filter((secondFilmList) => secondFilmList.title === leavingFilm.title);
            if (result.length > 0) {
                intersectionResults.push(result[0]);
            }
        }
    }
    return {
        intersectionResults: intersectionResults,
        leavingFilmsListLength: leavingFilmsList.container.length,
        secondListLength: secondList === null || secondList === void 0 ? void 0 : secondList.container.length,
        secondListType: secondList === null || secondList === void 0 ? void 0 : secondList.type,
    };
}
function emptyListHandler(leavingFilmsListLength, secondListLength, type) {
    let noListCategory = [];
    if (leavingFilmsListLength < 1) {
        noListCategory.push('Leaving');
    }
    if (secondListLength && secondListLength < 1) {
        switch (type) {
            case FilmType_1.FilmType.LEAVING:
                noListCategory.push('Leaving');
                break;
            case FilmType_1.FilmType.POPULARNOW:
                noListCategory.push('Popular Now');
                break;
            case FilmType_1.FilmType.ALLTIMEFAVORITES:
                noListCategory.push('All Time Favorites');
                break;
            default:
                break;
        }
    }
    return noListCategory;
}
function generateFilmTypeString(secondListType) {
    let filmTypeString = "";
    if (secondListType) {
        switch (secondListType) {
            case FilmType_1.FilmType.POPULARNOW:
                filmTypeString = "Popular Now";
                break;
            case FilmType_1.FilmType.ALLTIMEFAVORITES:
                filmTypeString = "All Time Favorites";
                break;
            default:
                break;
        }
    }
    return filmTypeString;
}
//# sourceMappingURL=filmService.js.map