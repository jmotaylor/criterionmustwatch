"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const FilmType_1 = require("../enums/FilmType");
const films_1 = require("../logic/films");
class FilmController {
    constructor() { }
    static async popularNow(req, res) {
        const results = await (0, films_1.getResults)(FilmType_1.FilmType.POPULARNOW);
        res.render('films-list', {
            movies: results.movies,
            type: results.filmType,
            noList: results.noList
        });
    }
    static async allTimeFavorites(req, res) {
        const results = await (0, films_1.getResults)(FilmType_1.FilmType.ALLTIMEFAVORITES);
        res.render('films-list', {
            movies: results.movies,
            type: results.filmType,
            noList: results.noList
        });
    }
}
exports.default = FilmController;
//# sourceMappingURL=films.js.map