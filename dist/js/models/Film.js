"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Review = exports.AllTimeFavoritesFilm = exports.PopularNowFilm = exports.LeavingFilm = void 0;
const FilmType_1 = require("../enums/FilmType");
class Film {
    constructor(title, link, image, review) {
        this.title = title;
        this.link = link;
        this.image = image;
        this.review = review;
    }
}
class LeavingFilm extends Film {
    constructor(title, link, image, review, type = FilmType_1.FilmType.LEAVING) {
        super(title, link, image, review);
        this.title = title;
        this.link = link;
        this.image = image;
        this.review = review;
        this.type = type;
    }
}
exports.LeavingFilm = LeavingFilm;
class PopularNowFilm extends Film {
    constructor(title, link, image, review, type = FilmType_1.FilmType.POPULARNOW) {
        super(title, link, image, review);
        this.title = title;
        this.link = link;
        this.image = image;
        this.review = review;
        this.type = type;
    }
}
exports.PopularNowFilm = PopularNowFilm;
class AllTimeFavoritesFilm extends Film {
    constructor(title, link, image, review, type = FilmType_1.FilmType.ALLTIMEFAVORITES) {
        super(title, link, image, review);
        this.title = title;
        this.link = link;
        this.image = image;
        this.review = review;
        this.type = type;
    }
}
exports.AllTimeFavoritesFilm = AllTimeFavoritesFilm;
class Review {
    constructor(title, link, percentage, freshness, type = FilmType_1.FilmType.REVIEWS) {
        this.title = title;
        this.link = link;
        this.percentage = percentage;
        this.freshness = freshness;
        this.type = type;
    }
}
exports.Review = Review;
//# sourceMappingURL=Film.js.map