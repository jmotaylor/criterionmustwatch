"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const urls_1 = require("../urls");
const FilmType_1 = require("../enums/FilmType");
const Category_1 = require("./Category");
class Categories {
    constructor(leavingFilms, popularNowFilms, allTimeFavoritesFilms, reviews) {
        this.leaving = new Category_1.Category(FilmType_1.FilmType.LEAVING, urls_1.leavingUrl, leavingFilms);
        this.popularNow = new Category_1.Category(FilmType_1.FilmType.POPULARNOW, urls_1.popularNowUrl, popularNowFilms);
        this.allTimeFavorites = new Category_1.Category(FilmType_1.FilmType.ALLTIMEFAVORITES, urls_1.allTimeFavoritesUrl, allTimeFavoritesFilms);
        this.reviews = new Category_1.Category(FilmType_1.FilmType.REVIEWS, urls_1.rottenTomatoesUrl, reviews);
    }
}
exports.default = Categories;
//# sourceMappingURL=Categories.js.map