export var MenuState;
(function (MenuState) {
    MenuState[MenuState["Open"] = 0] = "Open";
    MenuState[MenuState["Toggling"] = 1] = "Toggling";
    MenuState[MenuState["Closed"] = 2] = "Closed";
})(MenuState || (MenuState = {}));
//# sourceMappingURL=enums.js.map