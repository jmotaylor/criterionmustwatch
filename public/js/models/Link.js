export default class Link {
    constructor(element, href) {
        this.element = element;
        this.href = href;
        this.attachEventListener();
    }
    attachEventListener() {
        this.element.addEventListener('click', this.showSpinnerAndRedirect.bind(this));
        this.element.addEventListener("keypress", this.handleEnterPressed.bind(this));
    }
    showSpinnerAndRedirect() {
        var _a;
        (_a = document.querySelector('body')) === null || _a === void 0 ? void 0 : _a.classList.remove('hide-spinner');
        const newUrl = this.createNewUrl();
        setTimeout(() => window.location.href = newUrl, 1000);
    }
    createNewUrl() {
        const pathname = window.location.pathname;
        const hash = window.location.hash;
        const pathnameWithHash = pathname + hash;
        return window.location.href.replace(pathnameWithHash, this.href);
    }
    handleEnterPressed(event) {
        if (event.key === "Enter") {
            this.showSpinnerAndRedirect();
        }
    }
}
//# sourceMappingURL=Link.js.map