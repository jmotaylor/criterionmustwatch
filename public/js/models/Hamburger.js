import Menu from "./Menu.js";
export default class Hamburger {
    constructor(element) {
        this.element = element;
        this.init();
        this.menu = new Menu(document.querySelector('.left-menu'));
    }
    init() {
        this.attachEventListener();
    }
    attachEventListener() {
        this.element.addEventListener('click', () => this.menu.toggleMenu());
    }
}
//# sourceMappingURL=Hamburger.js.map