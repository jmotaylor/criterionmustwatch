const baseLeavingUrl = 'https://www.criterionchannel.com/leaving-';

function generateLeavingUrl() {
  const lastDayDate = getLastDayOfTheCurrentMonth();
  
  return baseLeavingUrl + lastDayDate + '?page=';
}

function getLastDayOfTheCurrentMonth():string {
  const year = new Date().getFullYear();
  const month = new Date().getMonth() + 1;
  const lastDate = new Date(year, month, 0);
  
  return lastDate.toLocaleString('en-US', {month: 'long', day: 'numeric'})
    .toLowerCase()
    .replace(/ /g,"-");
}

export const popularNowUrl = 'https://www.criterionchannel.com/popular-now?page=';
export const allTimeFavoritesUrl = 'https://www.criterionchannel.com/all-time-favorites?page=';
export const leavingUrl = generateLeavingUrl();
export const rottenTomatoesUrl = 'https://www.rottentomatoes.com/search?search=';