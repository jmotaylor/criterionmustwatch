import { PopularNowFilm, LeavingFilm, AllTimeFavoritesFilm } from '../models/Film';
import { FilmType } from '../enums/FilmType';
import Categories from '../models/Categories';
import createCriterionLists from '../scrapers/criterion';
import getReviews from '../scrapers/reviews';
import { Category } from '../models/Category';
import lodash from 'lodash';
import _ from 'lodash';

export async function getResultsWithReviews() {
  let categories = new Categories([],[],[],[])

  await createCriterionLists(categories.leaving);

  const leavingList = categories.leaving.container;

  await getReviews(categories.reviews, leavingList);

  const leavingListWithReviews = createListWithReviews(categories);

  const filmTypeString = generateFilmTypeString(categories.leaving.type);

  return { 
    movies: leavingListWithReviews,
    filmType: filmTypeString,
    noList: leavingListWithReviews.length < 1 ? ['Leaving'] : []
  };
}

function createListWithReviews(categories: Categories) {
  categories.leaving.container.forEach((film: LeavingFilm) => {
    film.review = lodash.find(categories.reviews.container, {title:film.title});
  });

  return lodash.orderBy(categories.leaving.container, [function(film:LeavingFilm) { return film.review?.percentage || '' }, 'title'], ['desc', 'asc']);
}

export async function getResults(type: FilmType)
  :Promise<{
    movies: (PopularNowFilm | LeavingFilm)[],
    filmType: string,
    noList: string[]
  }> {  
  const results = await generateIntersectionList(type);
  
  const emptyLists = emptyListHandler(
    results.leavingFilmsListLength,
    results.secondListLength,
    results.secondListType
  );

  const filmTypeString = generateFilmTypeString(results.secondListType);
  
  return { 
    movies: results.intersectionResults,
    filmType: filmTypeString,
    noList: emptyLists
  };
}

async function generateIntersectionList(type: FilmType)
  :Promise<{
    intersectionResults: (LeavingFilm | PopularNowFilm | AllTimeFavoritesFilm)[],
    leavingFilmsListLength: number,
    secondListLength: number | undefined,
    secondListType: FilmType | undefined
  }> {
  let categories = new Categories([],[],[],[])
  
  await getFilms(type, categories);

  const filteredResults = filterFilms(type, categories);

  return {
    intersectionResults: filteredResults.intersectionResults,
    leavingFilmsListLength: filteredResults.leavingFilmsListLength,
    secondListLength: filteredResults.secondListLength,
    secondListType: filteredResults.secondListType
  };
}

async function getFilms(type: FilmType, categories:Categories):Promise<void> {
  // Clear containers because even after refreshing, the old content is retained
  for (const category in categories) {
    categories[category as keyof typeof categories].container = [];
  }
  
  const leavingCategory = categories.leaving;

  await createCriterionLists(leavingCategory);

  const secondCategory = type === FilmType.POPULARNOW
    ? categories.popularNow
    : categories.allTimeFavorites;

  await createCriterionLists(secondCategory);
}

function filterFilms(type:FilmType, categories:Categories) {

  let intersectionResults:(LeavingFilm | PopularNowFilm | AllTimeFavoritesFilm)[] = [];
  let secondList:Category | null
  const leavingFilmsList = categories.leaving;

  switch (type) {
    case FilmType.POPULARNOW:
      secondList = categories.popularNow;
      break;

    case FilmType.ALLTIMEFAVORITES:
      secondList = categories.allTimeFavorites;
      break;
  
    default:
      secondList = null
      break;
  }

  if (leavingFilmsList.container.length > 0 && secondList && secondList.container.length > 0) {
    for (const leavingFilm of leavingFilmsList.container) {
      const result = secondList.container.filter((secondFilmList) => secondFilmList.title === leavingFilm.title);
      
      if(result.length > 0) {
        intersectionResults.push(result[0]);
      }
    }
  } 

  return {
    intersectionResults: intersectionResults,
    leavingFilmsListLength: leavingFilmsList.container.length,
    secondListLength: secondList?.container.length,
    secondListType: secondList?.type,
  };
} 

function emptyListHandler(
  leavingFilmsListLength: number, 
  secondListLength: number | undefined,
  type: FilmType | undefined): string[]
{
  let noListCategory: string[] = [];

  if (leavingFilmsListLength < 1) {
    noListCategory.push('Leaving');
  }

  if (secondListLength && secondListLength < 1) {
    switch (type) {
      case FilmType.LEAVING:
        noListCategory.push('Leaving');
        break;

      case FilmType.POPULARNOW:
        noListCategory.push('Popular Now');
        break;

      case FilmType.ALLTIMEFAVORITES:
        noListCategory.push('All Time Favorites');
        break;
    
      default:
        break;
    }      
  }

  return noListCategory;
}

function generateFilmTypeString(secondListType:FilmType | undefined):string {
  let filmTypeString = "";
  
  if (secondListType) {
    switch (secondListType) {
      case FilmType.POPULARNOW:
        filmTypeString = "Popular Now";
        break;
  
      case FilmType.ALLTIMEFAVORITES:
        filmTypeString = "All Time Favorites";
        break;
    
      default:
        break;
    }
  }

  return filmTypeString;
}
