import axios from 'axios';
import cheerio from 'cheerio';
import { FilmType } from '../enums/FilmType';
import { Category } from '../models/Category';
import { AllTimeFavoritesFilm, LeavingFilm, PopularNowFilm } from '../models/Film';

export default async function createLists(category: Category): Promise<void> {
  resetFlags();

  while (hasMorePages) {
    try {
      await scrapeFilms(category);
      page++;
    } catch (err) {
      console.error('oopsie');
    }
  } 
}

let page = 1;
let hasMorePages = true;

function resetFlags() {
  page = 1;
  hasMorePages = true;
}

async function scrapeFilms(
  category:{
    type:FilmType,
    url:string,
    container:(PopularNowFilm | LeavingFilm | AllTimeFavoritesFilm)[]
  }):Promise<void> {
  try {
    const { data } = await axios.get(category.url + page);
    const $ = cheerio.load(data);
    const filmItems = $(".js-collection-item");

    filmItems.each((_:number, el:cheerio.Element) => {
      const film = assignValuesToFilm($, el, category.type);

      if (film) {
        category.container.push(film);
      } else {
        throw('FilmType is unknown.');
      }
    });

    // The site will have a "load more" button if there are more films to scrape
    hasMorePages = $('.js-load-more-link').length > 0 ? true: false;
  } catch (err) {
    hasMorePages = false;
    console.error('an error has occurred', err);
  }  
}

function assignValuesToFilm(
  $:cheerio.Root,
  el:cheerio.Element,
  type: FilmType
):LeavingFilm | PopularNowFilm | AllTimeFavoritesFilm | null {    
  const title = ($(el).find(".browse-item-title").find('strong').text()).trim();
  const link = $(el).find(".browse-item-title").find('a').attr('href');
  const image = $(el).find("img").attr('src');

  let film:LeavingFilm | PopularNowFilm | AllTimeFavoritesFilm | null;

  switch (type) {
    case FilmType.LEAVING:
      film = new LeavingFilm(title, link, image);  
      break;

    case FilmType.POPULARNOW:
      film = new PopularNowFilm(title, link, image);  
      break;

    case FilmType.ALLTIMEFAVORITES:
      film = new AllTimeFavoritesFilm(title, link, image);  
      break;
  
    default:
      film = null;
      break;
  }
  
  return film;
}