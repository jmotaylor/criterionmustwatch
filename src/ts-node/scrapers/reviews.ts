import axios from 'axios';
import cheerio from 'cheerio';
import { FilmType } from '../enums/FilmType';
import { Category } from '../models/Category';
import { Review } from '../models/Film';

export default async function createLists(category: Category, leavingList: Category["container"]): Promise<void> {
  try{
    const leavingFilmTitles = leavingList.map(film => film.title);
    
    await scrapeFilms(category, leavingFilmTitles);
  } catch (err) {
    console.error('oopsie');
  }
}

async function scrapeFilms(category:{
  type:FilmType,
  url:string,
  container:Review[]
}, titles:string[]):Promise<void> {
  try {
    for(const title of titles) {
      const encodedTitle = encodeURIComponent(title);
      const url = category.url + encodedTitle;
      const { data } = await axios.get(url);
      const $ = cheerio.load(data);
      const reviewItems = $('search-page-result[slot="movie"] search-page-media-row');
 
      reviewItems.each((_:number, el:cheerio.Element) => {
        const reviewTitle = ($(el).find('a[slot="title"]').text()).trim();

        if (reviewTitle == title) {
          // return only the very last film among duplicates because that's the oldest of the duplicates and is likely what's on criterion
          const newReviewsContainer = removeDuplicates(category.container, title);

          category.container = newReviewsContainer;

          const review = assignValuesToReview($, el);
          category.container.push(review);
        }
      });      
    }
  } catch (err) {
    console.error('an error has occurred', err);
  }  
}

function removeDuplicates(reviews: Review[], title: string): Review[] {
  for (let i = 0; i < reviews.length; i++) {
    if (reviews[i].title == title) {
      reviews.splice(i, 1);
    }
  }

  return reviews;
}

function assignValuesToReview($:cheerio.Root, el:cheerio.Element): Review {
  const title = ($(el).find('a[slot="title"]').text()).trim();
  const href = $(el).find('a[slot="thumbnail"]').attr('href');
  const percentageString = $(el).attr('tomatometerscore');
  const freshness = $(el).attr('tomatometerstate');
  
  // Need to convert to int to be able to use lodash orderby descending properly
  const percentage = percentageString ? parseInt(percentageString) : null;

  return new Review(title, href, percentage, freshness);
}