import express from 'express';
import path from 'path';
import filmsRoute from './routes/filmRoutes';

const app = express();
const {
  PORT = process.env.PORT || 3000,
} = process.env;

// app.set('views', __dirname+'/src/views/');
app.set('view engine', 'pug');

app.use(express.static(path.join(__dirname, '../../public')));

app.use('/', filmsRoute);

app.listen(PORT, () => {
  console.log('server started at http://localhost:' + PORT);
});