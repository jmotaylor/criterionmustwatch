import { FilmType } from '../enums/FilmType';
import { LeavingFilm, PopularNowFilm, AllTimeFavoritesFilm, Review } from './Film';

export class Category {
  constructor(
    public type: FilmType,
    public url: string,
    public container: (LeavingFilm | PopularNowFilm | AllTimeFavoritesFilm | Review)[] | []
  ) {}
}