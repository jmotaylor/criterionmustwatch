import { FilmType } from '../enums/FilmType';

class Film {
  constructor(
    public title: string,
    public link?: string,
    public image?: string,
    public review?: Review 
  ) {}
}

export class LeavingFilm extends Film{
  constructor(
    public title: string,
    public link?: string,
    public image?: string,
    public review?: Review,
    public type: FilmType = FilmType.LEAVING
  ) {
    super(title, link, image, review);
  }
}

export class PopularNowFilm extends Film{
  constructor(
    public title: string,
    public link?: string,
    public image?: string,
    public review?: Review,
    public type: FilmType = FilmType.POPULARNOW
  ) {
    super(title, link, image, review);
  }
}

export class AllTimeFavoritesFilm extends Film{
  constructor(
    public title: string,
    public link?: string,
    public image?: string,
    public review?: Review,
    public type: FilmType = FilmType.ALLTIMEFAVORITES
  ) {
    super(title, link, image, review);
  }
}

export class Review {
  constructor(
    public title: string,
    public link?: string,
    public percentage?: number | null,
    public freshness?: string,
    public type: FilmType = FilmType.REVIEWS
  ) {}
}