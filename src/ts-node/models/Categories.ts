import { AllTimeFavoritesFilm, LeavingFilm, PopularNowFilm, Review } from "./Film";
import { popularNowUrl, leavingUrl, allTimeFavoritesUrl, rottenTomatoesUrl } from '../urls';
import { FilmType } from "../enums/FilmType";
import { Category } from "./Category";

export default class Categories {
  public leaving:Category;
  public popularNow:Category;
  public allTimeFavorites:Category;
  public reviews:Category;

  constructor(leavingFilms:LeavingFilm[] | [],
    popularNowFilms:PopularNowFilm[] | [],
    allTimeFavoritesFilms:AllTimeFavoritesFilm[] | [],
    reviews:Review[] | [],
  ) {
    this.leaving = new Category(FilmType.LEAVING, leavingUrl, leavingFilms);
    this.popularNow = new Category(FilmType.POPULARNOW, popularNowUrl, popularNowFilms);
    this.allTimeFavorites = new Category(FilmType.ALLTIMEFAVORITES, allTimeFavoritesUrl, allTimeFavoritesFilms);
    this.reviews = new Category(FilmType.REVIEWS, rottenTomatoesUrl, reviews);
  }
}