import express from 'express';
import { Request, Response } from 'express';
import FilmController from '../controllers/FilmController';

const router = express.Router();

export default router.get("/", (req: Request, res: Response) => {
  res.render('index');
});

router.get("/popular-now", FilmController.popularNow);
router.get("/all-time-favorites", FilmController.allTimeFavorites);
router.get("/reviews", FilmController.reviews);
