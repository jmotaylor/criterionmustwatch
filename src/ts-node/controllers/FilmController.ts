import { Request, Response } from 'express';
import { FilmType } from "../enums/FilmType";
import { getResults, getResultsWithReviews } from "../services/filmService";

export default class FilmController {
  constructor() {}

  static async popularNow(req: Request, res: Response) {
    const results = await getResults(FilmType.POPULARNOW);

    res.render('films-list', { 
      movies: results.movies,
      header: 'Leaving / Popular Now',
      description: 'Films categorized as "Popular Now" that are leaving at the end of the month.',
      noList: results.noList
    });
  }

  static async allTimeFavorites(req: Request, res: Response) {
    const results = await getResults(FilmType.ALLTIMEFAVORITES);

    res.render('films-list', { 
      movies: results.movies,
      header: 'Leaving / All Time Favorites',
      description: 'Films categorized as "All Time Favorites" that are leaving at the end of the month.',
      noList: results.noList
    });
  }

  static async reviews(req: Request, res: Response) {
    const results = await getResultsWithReviews();

    res.render('films-list', { 
      movies: results.movies,
      header: 'Leaving Films With Tomatometer',
      description: 'Films leaving at the end of the month with scores from Rotten Tomatoes.',
      noList: results.noList
    });
  }
}