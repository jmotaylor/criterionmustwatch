import Link from "./models/Link.js";

const body = document.querySelector('body');

body?.classList.add('hide-spinner');

const linkArray = [
  new Link(document.querySelector('.popular-now')!, '/popular-now'),
  new Link(document.querySelector('.all-time-favorites')!, '/all-time-favorites'),
  new Link(document.querySelector('.reviews')!, '/reviews')
];
