export default class Link {
  constructor(public element:HTMLAnchorElement, public href:string) {
    this.attachEventListener();
  }

  private attachEventListener() {
    this.element.addEventListener('click', this.showSpinnerAndRedirect.bind(this));
    this.element.addEventListener("keypress", this.handleEnterPressed.bind(this));
  }

  private showSpinnerAndRedirect() {
    document.querySelector('body')?.classList.remove('hide-spinner');

    const newUrl = this.createNewUrl();

    setTimeout(() => window.location.href = newUrl, 1000);
  }

  private createNewUrl() {
    const pathname = window.location.pathname;
    const hash = window.location.hash;
    const pathnameWithHash = pathname + hash;

    return window.location.href.replace(pathnameWithHash, this.href);
  }

  private handleEnterPressed(event:KeyboardEvent) {
    if (event.key === "Enter") {
      this.showSpinnerAndRedirect();
    }    
  }
}





