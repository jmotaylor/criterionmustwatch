import { MenuState } from "../enums.js";

export default class Menu {
  public state:MenuState;

  constructor(public element:HTMLDivElement) {
    this.state = MenuState.Closed;
  }

  public toggleMenu() {
    switch (this.state) {
      case MenuState.Closed:
        this.element.classList.add('open');
        this.state = MenuState.Open;
        break;
      
      case MenuState.Open:
        this.element.classList.remove('open');
        this.state = MenuState.Closed;
        break;
    
      case MenuState.Toggling:
      default:
        break;
    }
  }
}