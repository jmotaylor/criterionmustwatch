import Menu from "./Menu.js";

export default class Hamburger {
  private menu:Menu;

  constructor(public element:HTMLDivElement) {
    this.init();
    this.menu = new Menu(document.querySelector('.left-menu')!);
  }
  
  private init():void {
    this.attachEventListener();
  }

  private attachEventListener() {
    this.element.addEventListener('click', () => this.menu.toggleMenu());
  }
}